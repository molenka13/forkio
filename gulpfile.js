const gulp = require('gulp')
const sass = require('gulp-sass')(require('sass'))
const browserSync = require('browser-sync').create()
const autoprefixer = require('gulp-autoprefixer')
const clean = require('gulp-clean')
const cleanCss = require('gulp-clean-css')
const concat = require('gulp-concat')
const imagemin = require('gulp-imagemin')
const jsMinify = require('gulp-js-minify')
const uglify = require('gulp-uglify')
const { series , task} = require('gulp')
const del = require('del');


function cleanDist() {
  return del(['./dist', '!./dist/img']);
}

// const cleanDist = () => {
//     return gulp
//     .src('./dist/' , {allowEmpty: true})
//     .pipe(clean())
// }
const compileScssInCss = () => {
    return gulp
    .src('./src/styles/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('./src/css/'))
}
const autoprefix = () => {
    return gulp
    .src('./src/styles/')
    .pipe(autoprefixer({
        grid: true,
        cascad:true,
    }))
}
const concatMinifyJs = () => {
    return gulp
    .src('./src/scripts/*.js')
    .pipe(concat('scripts.min.js'))
    .pipe(jsMinify())
    .pipe(gulp.dest('./src/scripts/'))
}
const concatMinifyCss = () => {
    return gulp
    .src('./src/css/*.css')
    .pipe(concat('styles.min.css'))
    .pipe(cleanCss())
    .pipe(gulp.dest('./src/css/'))
}
const copyCssToDist = () => {
    return gulp 
    .src('./src/css/styles.min.css' , {allowEmpty: true})
    .pipe(gulp.dest('./dist/'))
    .pipe(browserSync.reload({stream: true}))
}
const copyJsToDist = () => {
    return gulp
    .src('./src/scripts/scripts.min.js' , {allowEmpty: true})
    .pipe(gulp.dest('./dist/'))
    .pipe(browserSync.reload({stream: true}))
}
const img = () => {
    return gulp
    .src('./src/img/**/*.+(jpeg|jpg|png|svg)')
    .pipe(imagemin())
    .pipe(gulp.dest('./dist/img/'))
}
const build = series(cleanDist , compileScssInCss , autoprefix , concatMinifyCss , concatMinifyJs , copyCssToDist , copyJsToDist , img)

const dev = () => {
    gulp.watch('./src/styles/*.scss', series(compileScssInCss , concatMinifyCss , copyCssToDist))
    gulp.watch('./src/scripts/*.js' , series(compileScssInCss ,concatMinifyJs , copyJsToDist))
}

exports.build = build
exports.dev = dev
exports.cleanDist = cleanDist
exports.compileScssInCss = compileScssInCss
exports.autoprefix = autoprefix
exports.concatMinifyCss = concatMinifyCss
exports.concatMinifyJs = concatMinifyJs
exports.copyCssToDist = copyCssToDist
exports.copyJsToDist = copyJsToDist
exports.img = img